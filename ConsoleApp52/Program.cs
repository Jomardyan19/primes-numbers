﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp52
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 109, 26, 45, 86, 126, 78, 32, 991 };

            for (int i = 0; i < arr.Length; i++)
            {
                int sum = 0;

                int a = arr[i];
                while (a != 0)
                {
                    int rem = a % 10;
                    a = a / 10;
                    sum += rem;
                }
                int r = sum;

                bool isPrime = true;

                if (r <= 1)
                    isPrime = false;

                else if (r % 2 == 0 && r != 2)
                    isPrime = false;

                else
                {
                    double num = Math.Sqrt(r);
                    for (int div = 3; div <= num; div += 2)
                    {
                        if (r % div == 0)
                        {
                            isPrime = false;
                            break;
                        }
                    }
                }
                if (isPrime)
                    Console.WriteLine(arr[i] + "=" + r + " is Prime");
            }

        }
    }
}
